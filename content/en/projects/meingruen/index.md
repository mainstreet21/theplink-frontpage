---
title: "meinGrün"
weight: 2
resources:
    - src: final_map.png
      params:
          weight: -100
---

## Project Goal
The aim of the project is the experimental development and testing of a novel infrastructure of services and the app "meinGrün" (myGreen), which leads to improved information on green spaces in cities.

Lead: Leibniz-Institut für ökologische Raumentwicklung e.V. (IÖR), Dresden

Partner: Deutsches Zentrum für Luft- und Raumfahrt e.V. (DLR), Oberpfaffenhofen, Institut für Kartographie (IFK), Technische Universität Dresden, Heidelberg Institute for Geoinformation Technology (HeiGIT), GIScience group, Universität Heidelberg, ISB Institut für Software-Entwicklung und EDV-Beratung AG, Karlsruhe, urbanista GmbH & Co. KG, Hamburg, Terra Concordia gUG, Berlin

Duration: 11/2018 - 04/2021

Link to the mFUND project (in German):
[https://www.bmvi.de/SharedDocs/DE/Artikel/DG/mfund-projekte/meingruen.html](https://www.researchgate.net/deref/https%3A%2F%2Fwww.bmvi.de%2FSharedDocs%2FDE%2FArtikel%2FDG%2Fmfund-projekte%2Fmeingruen.html)

- Lab: [Dirk Burghardt's Lab](https://www.researchgate.net/lab/Dirk-Burghardt-Lab)