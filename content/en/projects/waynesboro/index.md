---
title: "Waynesboro (mainstreet21)"
weight: 1
resources:
    - src: waynesboro.png
      params:
          weight: -100
---

Social media has become a valuable source of data for urban planners to gather critical information. In the project, we propose to use analytical and mapping techniques to unearth the potentials of Location Based Social Networks (LBSNs) by extracting critical information for community design and planning in Waynesboro, VA.

**Project Leadership**
**UVA School of Architecture**: Guoping Huang, Mona El Khafif
**Institute of Cartography, TU Dresden**: Alexander Dunkel
**Waynesboro**: Luke Juday