---
title: "Collaboration"
weight: 30
---

<div style='display: grid; 
            text-align: left;
            '>
  <table cellspacing="0" cellpadding="0" style='border:none;'>
    <tr style='background: transparent;'>
      <td style='border:none; background: transparent; width: 33%'><img src="{{< baseurl >}}img/assets_images_gitlab-vgiscience.svg" height="190px" alt="VGIScience" style="border: 0;"/></td>
      <td style='border:none; background: transparent; width: 33%'><img src="{{< baseurl >}}img/University_of_Virginia_Rotunda_logo.svg" height="168px" alt="UVA" style="border: 0;"/></td>
      <td style='border:none; background: transparent; width: 33%'><img src="{{< baseurl >}}img/Logo_TU_Dresden.svg" height="168px" alt="UVA" style="border: 0;"/></td>
    </tr>
    <tr>
      <th style='border:none; width: 33%; background: transparent'><a href="https://www.vgiscience.org/">VGIscience.org</a></th>
      <th style='border:none; width: 33%; background: transparent'><a href="https://www.arch.virginia.edu/">UVA School of Architecture</a></th>
      <th style='border:none; width: 33%; background: transparent'><a href="https://tu-dresden.de/bu/umwelt/geo/ifk/">TUD Institute of Cartography</a></th>
    </tr>
  </table>
</div>