---
title: "About"
image: "profile.png"
weight: 8
---

We're a group of enthusiasts from **landscape and urban planning**, **data science** and **system engineering**, who work on open collaboration and **spatial** knowledge discovery tools for application to the fields of public planning and **citizen science**. [theplink.org]([http://theplink.org](http://theplink.org/)) is a staging area for demonstration of our tools such as [thePlink-docker](<https://gitlab.vgiscience.de/lbsn/theplink-docker>), an integrated spatial discourse ecosystem made for self-hosting. The **P** in the[P](http://theplink.org)link may refer to Planning, Projects, People, or Participation; or Personal & Public Values, Perspectives; or Perception.

### Features

* Entirely build using open source software
* Ready-made [docker containers](https://www.docker.com/) for self-hosting, _local knowledge stays local_
* Spatial Discussions for your community based on [Discourse](<https://www.discourse.org/>)
* Progressive Web App with [Vue.js](<https://vuejs.org/>) Frontend and [Mapbox GL JS](<https://github.com/mapbox/mapbox-gl-js>)
* Privacy-by-Design: Data Abstraction using [HyperLogLog](<https://github.com/citusdata/postgresql-hll>)
* Map Interface binds to project area, ability to add sub-projects and additional spatial information
* Scales easily: based on [fastapi](<https://github.com/tiangolo/fastapi>) and [dockerswarm.rocks](<https://dockerswarm.rocks/>)
