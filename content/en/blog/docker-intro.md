---
title: "Docker Template"
date: 2019-05-14T15:21:10+02:00
tags: ["git", "init", "docker"]
---

Theplink demonstration site went online and we're working to get our initial docker network running. This will be used as a demonstration setup for our case studies. [Theplink-docker repository](<https://gitlab.vgiscience.de/lbsn/theplink-docker>) is currently invite only. As soon as the first running version is available we will share it here.

