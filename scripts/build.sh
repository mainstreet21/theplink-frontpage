#! /usr/bin/env sh

# Exit in case of error
set -e

TAG=${TAG} \
BASE_URL=${BASE_URL}

if [ $TAG = "pages" ]; then
    # no need for multi stage build with nginx
    # gitlab pages has its own webserver
    # limit dockerimage to build-stage
    docker build -t ${DOCKER_IMAGE_BASE}:${TAG} . --target=build-stage
else
    docker build -t ${DOCKER_IMAGE_BASE}:${TAG} .
fi


