#! /usr/bin/env sh

# Exit in case of error
set -e

TAG=${TAG} \
BASE_URL=${BASE_URL} \
source ./scripts/build.sh

docker push ${DOCKER_IMAGE_BASE}:${TAG}
