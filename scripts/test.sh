#! /usr/bin/env sh

# Exit in case of error
set -e

TAG=${TAG} \
BASE_URL=${BASE_URL} \
docker-compose \
-f docker-compose.deploy.build.yml \
config > docker-stack.yml

docker-compose -f docker-stack.yml build
docker-compose -f docker-stack.yml down -v --remove-orphans # Remove possibly previous broken stacks left hanging after an error
docker-compose -f docker-stack.yml up -d
docker-compose -f docker-stack.yml down -v --remove-orphans
